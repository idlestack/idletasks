//
//  AddTaskVC.swift
//  IdleTasks
//
//  Created by Douglas Spencer on 12/5/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import Speech

class AddTaskVC: UIViewController, SFSpeechRecognizerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var txtTaskTitle: TextFieldSC!
    @IBOutlet weak var switchIsImportant: UISwitch!
    @IBOutlet weak var txtVwTaskDetails: TextViewVC!
    
    @IBOutlet weak var imgTitleMic: UIImageView!
    @IBOutlet weak var imgDescriptionMic: UIImageView!
    
    @IBOutlet weak var activityTitle: UIActivityIndicatorView!
    @IBOutlet weak var activityDescription: UIActivityIndicatorView!
    
    
    
    
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    func requestSpeechAcces() {
        SFSpeechRecognizer.requestAuthorization { authstatus in
            if authstatus == SFSpeechRecognizerAuthorizationStatus.authorized {
                
            }
        }
    }

    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        requestSpeechAcces()
    }
    
    func dismissKeyBoard() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.hideKeyboardWhenTappedAround()
    
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func btnAddTask(sender: UIButton) {
        let dbcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let idletask = Task(context: dbcontext)
        
        idletask.isImportant = switchIsImportant.isOn
        idletask.isVoiceRelated = false
        idletask.tasktitle = txtTaskTitle.text
        idletask.taskdetails = txtVwTaskDetails.text
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        navigationController?.popViewController(animated: true)
        
    }
    
    func GatherTaskTitleFromMic() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        } else {
            RecordAudio(type: "title")
        }

    }
    
    func GatherTaskDetailsFromMic() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        } else {
            RecordAudio(type: "details")
        }

    }
    
    func RecordAudio(type: String) {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                // use this:
                
                if type == "details" {
                    self.txtVwTaskDetails.text = result?.bestTranscription.formattedString
                } else {
                    self.txtTaskTitle.text = result?.bestTranscription.formattedString
                }
                
                self.activityDescription.stopAnimating()
                self.activityTitle.stopAnimating()

                
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
                    } catch {
        }

    }
    
        
    @IBAction func GetTitleFromMic() {
        activityTitle.startAnimating()
        GatherTaskTitleFromMic()
    }
    
    @IBAction func GetDescFromMic() {
        activityDescription.startAnimating()
        GatherTaskDetailsFromMic()
    }

}
