//
//  TaskDetailVC.swift
//  IdleTasks
//
//  Created by Douglas Spencer on 12/6/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import CoreData

class TaskDetailVC: UIViewController {
    
    let dbcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var taskTitle: String = ""
    var taskDetails: String = ""
    var taskImportance: Bool = false
    
    @IBOutlet weak var txtTaskTitle: TextFieldSC!
    @IBOutlet weak var txtTaskDetails: TextViewVC!
    @IBOutlet weak var switchImportance: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtTaskTitle.text = taskTitle
        txtTaskDetails.text = taskDetails
        switchImportance.isOn = taskImportance

        self.hideKeyboardWhenTappedAround()
        self.automaticallyAdjustsScrollViewInsets = false
    
    }
}
