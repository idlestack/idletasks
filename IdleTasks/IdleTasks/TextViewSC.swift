//
//  TextViewSC.swift
//  IdleTasks
//
//  Created by Douglas Spencer on 12/5/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit

@IBDesignable
class TextViewVC : UITextView {
    
    @IBInspectable var BorderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = BorderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var BGColor: UIColor? {
        didSet {
            layer.backgroundColor = BGColor?.cgColor
        }
    }
    
    @IBInspectable var CornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = CornerRadius
        }
    }
}

