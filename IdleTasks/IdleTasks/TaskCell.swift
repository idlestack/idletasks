//
//  TaskCell.swift
//  IdleTasks
//
//  Created by Douglas Spencer on 12/6/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    
    @IBOutlet weak var taskText: UILabel!
    @IBOutlet weak var importantImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func UpdateUI(text: String, isImportant: Bool) {
        
        if isImportant {
            importantImage.image = UIImage(named: "important")
        }
        
        taskText?.text = text
        
    }
}
