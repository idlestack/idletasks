//
//  ViewController.swift
//  IdleTasks
//
//  Created by Douglas Spencer on 12/5/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblViewTasks: UITableView!
    
    var myTasks: [Task] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tblViewTasks.delegate = self
        tblViewTasks.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        
        tblViewTasks.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let taskCell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskCell {
            let mytask = myTasks[indexPath.row]

            taskCell.UpdateUI(text: mytask.tasktitle!, isImportant: mytask.isImportant)
            return taskCell
    
        } else {
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "TaskDetail", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myTasks.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let dbcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        if editingStyle == .delete {
            let task = myTasks[indexPath.row]
            dbcontext.delete(task)
        }
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        getData()
        
        tblViewTasks.reloadData()

    }
    
    func getData() {
        
       let dbcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do {
            try myTasks = dbcontext.fetch(Task.fetchRequest())
        } catch  {
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TaskDetail") {
            let controller: TaskDetailVC = segue.destination as! TaskDetailVC
            print(sender)
            //let row = (sender as! NSIndexPath).row; //we know that sender is an NSIndexPath here.
            let myTask = myTasks[sender as! Int]
            
            controller.taskDetails = myTask.taskdetails!
            controller.taskImportance = myTask.isImportant
            controller.taskTitle = myTask.tasktitle!
            
        }
    }
}

